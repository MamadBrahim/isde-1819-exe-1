import numpy as np
import warnings


def split_data(X, y, tr_fraction=0.6):
    """
    Split the data X,y into two random subsets

    """
    num_samples = y.size
    n_tr = int(num_samples * tr_fraction)

    idx = np.array(range(0, num_samples))
    np.random.shuffle(idx)  # shuffle the elements of idx

    tr_idx = idx[0:n_tr]
    ts_idx = idx[n_tr:]

    Xtr = X[tr_idx, :]
    ytr = y[tr_idx]

    Xts = X[ts_idx, :]
    yts = y[ts_idx]

    return Xtr, ytr, Xts, yts


def count_digits(y):
    """Count the number of elements in each class."""
    classes = np.unique(y)
    print y
    print 'y (unique): ', np.unique(y)
    simple_number = np.unique(y).size
    c = np.zeros(shape=(simple_number,), dtype=int)

    for i in xrange(simple_number):
        c[i] = np.sum(y == classes[i])
    print 'counts: ', c
    return c


def fit(Xtr, ytr):
    """Compute the average centroid for each class."""
    num_classes = np.unique(ytr).size
    centroids = np.zeros(shape=(num_classes, Xtr.shape[1]))
    for k in xrange(num_classes):
        xk = Xtr[ytr == k, :]
        with warnings.catch_warnings():
            warnings.simplefilter("ignore", category=RuntimeWarning)
            centroids[k, :] = np.mean(xk, axis=0)
    return centroids
